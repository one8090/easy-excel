package com.husky.easyexcle.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.fastjson.JSON;
import com.husky.easyexcle.entity.ExcelDateEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author husky
 * @Date 2022/3/14 16:13
 * @Description: easyExcel 工具类
 **/
@Slf4j
public class ExcleUtil {

    /**
     * 读取Excel文件数据
     * @param fileName excel 文件名
     */
    public <T> void readExcel(String fileName, Class<T> tclass) {
        //获取根目录
        ApplicationHome ah = new ApplicationHome(this.getClass());
        String excelPath = ah.getSource().getParentFile().toString()+"/excel/";
        log.info("excelPath={}",excelPath);
        ExcelReader excelReader = null;
        AtomicInteger count = new AtomicInteger();
        try {

            EasyExcel.read(excelPath+fileName, ExcelDateEntity.class, new PageReadListener<T>(
                    dataList -> {
                        for (T demoData : dataList) {

                        }
                        log.info("Excel内容{}", JSON.toJSONString(dataList));
                        count.set(dataList.size());
                        log.info("Excel内容数量：{}",count);
                    }
            )).sheet(0).doRead();
        }catch (Exception e){
            log.error("Excel PageReadListener 读取出错。Exception={}",e);
        }finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
    }

    /**
     * 将数据写入Excel
     * @param fileName 文件名
     * @param sheetName 工作簿名称
     * @param dataList 带写入数据
     */
    public <T> void exportExcle(String fileName,String sheetName,List<T> dataList, Class headClass){
        //获取根目录
        ApplicationHome ah = new ApplicationHome(this.getClass());

        //excle 导出文件夹
        String excelPath = ah.getSource().getParentFile().toString()+"/excel/";

        //根据年月日创建文件夹
        String timePath = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).replace("-","/")+"/";
        log.info("excelPath={}",excelPath);
        //如果没有文件夹则新建
        File filePath = new File(excelPath  + timePath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }

        EasyExcel.write(excelPath+timePath+fileName, headClass)
                .sheet(sheetName)
                .doWrite(() -> {
                    log.info("待导出数据={}",dataList);
                     return dataList;
                });
    }

    public static void main(String[] args) {
        List<ExcelDateEntity> list = new CopyOnWriteArrayList<>();
        ExcelDateEntity excelDateEntity = new ExcelDateEntity();
        excelDateEntity.setAge(19);
        excelDateEntity.setBirthday(LocalDateTime.now());
        excelDateEntity.setMarriedFlag(true);
        excelDateEntity.setName("林婉儿");
        excelDateEntity.setPartyMemberFlag(1);
        excelDateEntity.setShowFlag("不展示");
        excelDateEntity.setSalary("300.2345");
        excelDateEntity.setStature("173");
        list.add(excelDateEntity);
       // new ExcleUtil().exportExcle("aaa.xlsx","我是工作簿",list,ExcelDateEntity.class);
        new ExcleUtil().readExcel("test.xlsx",excelDateEntity.getClass());
    }

    public static <T> List<T> readWebExcelData(MultipartFile file, Class<T> tclass) {
        List<T> resultList = new CopyOnWriteArrayList<>();
        ExcelReader excelReader = null;
        AtomicInteger count = new AtomicInteger();
        try {
            EasyExcel.read(file.getInputStream(), tclass, new PageReadListener<T>(
                    dataList -> {
                        for (T demoData : dataList) {
                            resultList.add(demoData);
                        }
                        log.info("Excel内容{}", JSON.toJSONString(dataList));
                        count.set(dataList.size());
                        log.info("Excel内容数量：{}",count);
                    }

            )).sheet(0).doRead();
            return resultList;
        }catch (Exception e){
            log.error("Excel PageReadListener 读取出错。Exception={}",e);
        }finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
        return resultList;
    }
}
