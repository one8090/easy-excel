package com.husky.easyexcle.enums;

/**
 * @Author husky
 * @Date 2022/3/15 13:23
 * @Description: 指定字符串转指定数字枚举类
 **/
public enum StringToIntegerConverterEnum {

    /**
     * 中文是
     */
    CHINESE_YES("是",1,"中文是"),

    /**
     * 中文否
     */
    CHINESE_NO("否",2,"中文否"),

    /**
     * 小写英文是
     */
    LOWERCASE_ENGLISH_YES("yes",2,"小写英文是"),

    /**
     * 小写英文否
     */
    LOWERCASE_ENGLISH_NO("no",2,"小写英文否"),
    ;

    /**
     * 带转换的字符串
     */
    private String key;

    /**
     * 带转换的字符串
     */
    private Integer value;

    /**
     * 备注，便于阅读
     */
    private String remark;

    /**
     * 私有构造,防止被外部调用
     * @param key 带转换的字符串
     * @param value 字符串转换的数字
     * @param remark 备注，便于阅读
     */
    private StringToIntegerConverterEnum(String key, int value,String remark){
        this.key=key;
        this.value=value;
        this.remark = remark;
    }

    /**
     * 获取键[带转换的字符串]
     * @return
     */
    public String getKey(){
        return this.key;
    }

    /**
     * 获取值[字符串转换的数字]
     * @return
     */
    public int getValue(){
        return this.value;
    }
}
