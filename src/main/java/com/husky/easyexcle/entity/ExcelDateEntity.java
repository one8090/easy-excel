package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.husky.easyexcle.annotation.ExportAppendStringAnnotation;
import com.husky.easyexcle.annotation.StringToBooleanConverterAnnotation;
import com.husky.easyexcle.annotation.StringToIntegerConverterAnnotation;
import com.husky.easyexcle.converter.CustomExportAppendStringConverter;
import com.husky.easyexcle.converter.CustomStringToBooleanConverter;
import com.husky.easyexcle.converter.CustomStringToIntegerConverter;
import com.husky.easyexcle.enums.StringToIntegerConverterEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.time.LocalDateTime;

/**
 * @Author husky
 * @Date 2022/3/14 16:18
 * @Description: Excel 数据封装实体
 **/
@ColumnWidth(15)  //列宽
@HeadRowHeight(20) //标题行高
@HeadFontStyle(fontHeightInPoints = 12, color = 9) //文件头的字体大小和颜色
@Data // lombok注解
@Slf4j
public class ExcelDateEntity {

    @ExcelProperty(value = "姓名")
    private String name;

    @ExcelProperty(value = "年龄")
    private Integer age;

    /**
     * 指定字符转转为布尔值
     */
    @StringToBooleanConverterAnnotation(trueStr = "已婚",falseStr = "未婚")
    @ExcelProperty(value = "婚姻状态",converter = CustomStringToBooleanConverter.class)
    private boolean marriedFlag;

    @ExportAppendStringAnnotation(endPositionContent = "万元/月")
    @ExcelProperty(value = "薪资",converter = CustomExportAppendStringConverter.class)
    private String salary;

    @ColumnWidth(20)  //列宽
    @ExcelProperty(value = "生日")
    @DateTimeFormat(value = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime birthday;

    /**
     * 指定字符串转为指定数字
     */
    @StringToIntegerConverterAnnotation(converterEnums = {StringToIntegerConverterEnum.CHINESE_NO,StringToIntegerConverterEnum.CHINESE_YES})
    @ExcelProperty(value = "是否党员",converter = CustomStringToIntegerConverter.class)
    private Integer partyMemberFlag;

    /**
     * NumberFormat 常用
     * “0”——表示一位数值，如没有，显示0。如“0000.0000”，整数位或小数位>4，按实际输出，<4整数位前面补0小数位后面补0，凑足4位。
     * “#”：无小数，小数部分四舍五入。
     * “.#”：整数部分不变，一位小数，四舍五入。
     * “.##”：整数部分不变，二位小数，四舍五入。
     * “.”——表示小数点。
     */
    @ExportAppendStringAnnotation(beginPositionContent = "TA", endPositionContent = "CM")
    @ExcelProperty(value = "身高",converter = CustomExportAppendStringConverter.class)
    @NumberFormat(value = ".#########")
    private String stature;

    /**
     * excel 匹配时忽略该字段
     */
    @ExcelIgnore
    @ExcelProperty(value = "爱好")
    private String showFlag;

}
