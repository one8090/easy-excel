package com.husky.easyexcle.controller;

import com.husky.easyexcle.entity.ExcelDateEntity;
import com.husky.easyexcle.service.ExcelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

/**
 * @Author husky
 * @Date 2022/3/22 15:58
 * @Description: Excel相关控制层
 **/

@RestController
@Slf4j
public class ExcelController {

    @Autowired
    private ExcelService excelServiceImpl;
    /**
     * 读取上传Excel数据
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping("read/excel/data")
    public String readExcelData(MultipartFile file) throws IOException {
        log.info("读取上传Excel开始。file={}",file);
      try {
          List<ExcelDateEntity> list = excelServiceImpl.readExcelData(file);
          log.info("list={}",list);
          return "SUCCESS";
      }catch (Exception e){
          log.error("读取上传Excel异常。Exception={}",e);
          return "FAIL";
      }

    }

}
